extern crate rand;
use rand::prelude::*;
use crate::tic_tac_toe::{Game, WinStatus};
use crate::nn::NeuralNet;

pub trait Player {
    fn take_turn(&self, board: &[i8; 9]) -> u8;
    fn player_name(&self) -> &str;
}

pub struct RandomPicker {
    pub name: String
}

impl Player for RandomPicker {
    fn player_name(&self) -> &str { &self.name }
    fn take_turn(&self, board: &[i8; 9]) -> u8 {
        *board
            .iter()
            .enumerate()
            .filter(|(_i, val)| **val == 0)
            .map(|(i, _val)| i as u8)
            .collect::<Vec<u8>>()
            .choose(&mut rand::thread_rng())
            .unwrap()
    }
}

#[derive(Clone, Debug)]
pub struct NeuralNetPlayer {
    pub name: String,
    pub net: NeuralNet,
    pub score: u32
}

impl Player for NeuralNetPlayer {
    fn player_name(&self) -> &str { &self.name }
    fn take_turn(&self, board: &[i8; 9]) -> u8 {
        let input: Vec<f32> = board.to_vec().iter().map(|i| *i as f32).collect();
        let results = crate::nn::calc(input, &self.net);
        let (choice, _) = board
            .iter()
            .zip(results.iter())
            .enumerate()
            .filter(|(_i, (&val, _result))| val == 0)
            .max_by(|(_, (_, &result_a)), (_, (_, result_b))|
                    result_a.partial_cmp(result_b).unwrap()
            )
            .unwrap();
        choice as u8
    }
}

pub fn completion_fitness(status: WinStatus, board: &[i8; 9]) -> u32 {
    let num_moves = board.iter().filter(|&x| *x != 0).count();
    match status {
        WinStatus::Winner => {
            //println!("win board: {:?}", board);
            (50f32 * (5.0/num_moves as f32)) as u32
        },
        WinStatus::Loser => 0,
        WinStatus::Draw => 10
    }
}

pub struct Blocker {
    pub player_number: i8
}

impl Player for Blocker {
    fn player_name(&self) -> &str { "Bob Blocker" }
    fn take_turn(&self, board: &[i8; 9]) -> u8 {
        let winning_move = crate::tic_tac_toe::potential_win(board, self.player_number);
        let blocking_move = crate::tic_tac_toe::potential_win(board, self.player_number*-1);
        let free_cells: Vec<u8> = board
            .iter()
            .enumerate()
            .filter(|(_i, &val)| val == 0)
            .map(|(i, _val)| i as u8)
            .collect();
        // let preferred_cells = vec![4, 0, 2, 6, 8, 1, 3, 5, 7];
        // let preferred_cell = *preferred_cells
        //     .iter()
        //     .find(|i| free_cells.contains(i))
        //     .unwrap();

        match winning_move {
            Some(pos) => pos,
            None => {
                match blocking_move {
                    Some(pos) => pos,
                    None => //preferred_cell
                        *free_cells.iter().choose(&mut rand::thread_rng()).unwrap()
                }
            }
        }
    }
}

pub struct HumanPlayer {}

impl Player for HumanPlayer {
    fn player_name(&self) -> &str { "Hoomanz" }
    fn take_turn(&self, board: &[i8; 9]) -> u8 {
        println!();
        draw_board(board);
        println!();
        println!("Player {} turn", self.player_name());
        get_move()
    }
}


pub fn print_winner(game: &Game, status: WinStatus) {
    match status {
        WinStatus::Winner => println!("Congrats player {} won!", icon(game.current_player)),
        WinStatus::Draw => println!("Draw :("),
        WinStatus::Loser => println!("Congrats player {} won!", icon(game.current_player*-1))
    }
    draw_board(&game.board);
}


fn get_move() -> u8 {
    let mut user_input=String::new();
    loop {
        print!("Enter position: ");
        std::io::stdin().read_line(&mut user_input).expect("Did not enter a correct string");
        match user_input.trim().parse::<u8>() {
            Ok(i) => {
                println!();
                break { convert_keyboard_to_move(i) }
            },
            _ => {
                println!("I couldn't read that.");
                user_input.clear();
            }
        }
    }
}

fn convert_keyboard_to_move(keypad_input: u8) -> u8 {
    match keypad_input {
        9 => 2,
        8 => 1,
        7 => 0,
        6 => 5,
        5 => 4,
        4 => 3,
        3 => 8,
        2 => 7,
        1 => 6,
        _ => 10
    }
}

fn draw_board(board: &[i8; 9]) {
    println!(" {} | {} | {} ", icon(board[0]), icon(board[1]), icon(board[2]));
    println!("---|---|---");
    println!(" {} | {} | {} ", icon(board[3]), icon(board[4]), icon(board[5]));
    println!("---|---|---");
    println!(" {} | {} | {} ", icon(board[6]), icon(board[7]), icon(board[8]));
}

fn icon(space: i8) -> &'static str {
    match space {
        1 => "X",
        -1 => "O",
        _ => " "
    }
}


