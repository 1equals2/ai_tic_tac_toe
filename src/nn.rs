extern crate rand;
extern crate serde_json;
extern crate serde;

use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::fs;

fn tanh(value: f32) -> f32 {
    (2.0 / (1.0 + std::f32::consts::E.powf(-2.0*value))) - 1.0
}

fn tanh_derivative(value: f32) -> f32 {
    1f32-(tanh(value).powf(2.0))
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct NeuralNet {
    pub layers: Vec<Layer>
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Layer {
    pub weights: Vec<Vec<f32>>
}

impl NeuralNet {
    pub fn write_to_file(&self, filename: &str) {
        let serialized = serde_json::to_string(self).expect("Unable to serialize Neural Net.");
        fs::write(filename, serialized).expect("Unabled to write serialized neural net to file.");
    }
}

pub fn random_net(inputs: usize, outputs:usize, inner_size: usize, layers: usize) -> NeuralNet {
    let layers: Vec<Layer> = if layers == 1 {
        vec! [ random_layer(inputs, outputs) ]
    } else {
        let mut l: Vec<Layer> = Vec::new();
        l.push(random_layer(inputs, inner_size));
        (1..layers-1)
            .into_iter()
            .for_each(|_| l.push(random_layer(inner_size, inner_size)));
        l.push(random_layer(inner_size, outputs));
        l
    };
    NeuralNet { layers: layers }
}

pub fn load_from_file(filename: &str) -> Result<NeuralNet, String> {
    match fs::read_to_string(filename) {
        Err(_) => Err(String::from("Unable to read Neural Net file.")),
        Ok(file_contents) =>
            match serde_json::from_str(file_contents.as_str()) {
                Err(_) => Err(String::from("Unable to deserialize Neural Net.")),
                Ok(net) => Ok(net)
            }
    }
}

pub fn calc(inputs: Vec<f32>, net: &NeuralNet) -> Vec<f32> {
    let mut inp = inputs;
    for l in net.layers.iter() {
        let out = calc_layer(inp, l);
        inp = out;
    }
    inp
}

fn calc_layer(inputs: Vec<f32>, layer: &Layer) -> Vec<f32> {
    layer.weights
        .iter()
        .map(|weights_iter|
             inputs
             .iter()
             .zip(weights_iter.iter())
             .map(|(input, weight)| input * weight)
             .fold(0f32, |sum, total| sum+total)
        )
        .map(|total| tanh(total))
        .collect()
}

pub fn random_layer(input_size: usize, output_size: usize) -> Layer {
    Layer { weights: random_weights(input_size, output_size) }
}

fn random_weights(input_size: usize, output_size: usize) -> Vec<Vec<f32>> {
    let mut rng = rand::thread_rng();
    (0..output_size)
        .into_iter()
        .map(|_|
             (0..input_size)
             .into_iter()
             .map(|_| rng.gen::<f32>())
             .collect()
        )
        .collect()
}


