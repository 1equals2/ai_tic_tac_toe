extern crate rayon;


mod tic_tac_toe;
mod player;
mod nn;

use crate::player::*;
use crate::tic_tac_toe::Game;
use crate::nn::{NeuralNet};
use rayon::prelude::*;
use std::env;

fn main() {
    let mut train = false;
    let args: Vec<String> = env::args().collect();
    let trained_filename = "ai.json";
    let population_size = 1000;
    let iterations = 50;

    if args.contains(&String::from("--train")) {
       train = true;
    }

    let mut best_net = match nn::load_from_file(trained_filename) {
        Ok(net) => net,
        Err(_) => random_nathan_net()
    };

    //  Game  ///////////////////////
    if train {
        loop {
            let new_best_net = train_existing(population_size, iterations, &vec![best_net]);
            new_best_net.write_to_file(trained_filename);
            best_net = new_best_net;
        }
    } else {
        let human = HumanPlayer {};
        let _randy = RandomPicker { name: String::from("Randy") };
        let nathan = NeuralNetPlayer { name: String::from("Nathan"), net: best_net, score: 0 };
        let _bob = Blocker { player_number: 1 };

        let mut game = Game::new();
        let (result, _board) = game.play_game(&nathan, &human);
        player::print_winner(&game, result.player1);
    }
}

fn random_nathan_net() -> NeuralNet { nn::random_net(9, 9, 18, 5) }

fn train_existing(population: u32, iterations: u32, existing_population: &Vec<NeuralNet>) -> NeuralNet {
    let starting_point = existing_population
        .iter()
        .map(|e| NeuralNetPlayer{
            name: String::from("AI"),
            net: e.clone(),
            score: 0
        })
        .collect();

    let best: NeuralNetPlayer = scored_population(population, &starting_point)
        .iter()
        .max_by(|a, b| a.score.cmp(&b.score))
        .unwrap()
        .clone();
    println!("Best score: {}; iterations to go: {}", best.score, iterations);
    if iterations == 0 {
        scored_population(population, &starting_point)
            .iter()
            .max_by(|a, b| a.score.cmp(&b.score))
            .unwrap()
            .clone().net
    } else {
        let mut keepers_tmp = scored_population(population, &starting_point);
        keepers_tmp.sort_by(|a, b| b.score.cmp(&a.score));
        let keepers = keepers_tmp
            .iter()
            .take((population as f32 *0.25) as usize)
            .map(|x| x.net.clone())
            .collect();
        train_existing(population, iterations-1, &keepers)
    }
}


fn scored_population(population_size: u32, existing_population: &Vec<NeuralNetPlayer>) -> Vec<NeuralNetPlayer> {
    let mut pop: Vec<NeuralNetPlayer> = (0..(population_size as usize - existing_population.len()))
        .into_iter()
        .map(|_|
             NeuralNetPlayer{
                 name: String::from("AI"),
                 net: random_nathan_net(),
                 score: 0
             })
        .collect();
    existing_population.iter().for_each(|e| pop.push(e.clone()));
    //let opp = pop.clone();
    let opp: Vec<Blocker> = pop.iter().map(|_| Blocker { player_number: -1 }).collect();

    for i in 0..pop.len() {
        //println!("Training #{}", i+1);
        let score = opp
            .par_iter()
            .map(|o| Game::new().play_game(&pop[i], o))
            .map(|(results, board)| player::completion_fitness(results.player1, &board))
            .sum();
        pop[i].score = score;
    }
    pop
}


