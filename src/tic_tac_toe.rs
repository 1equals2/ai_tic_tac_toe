use crate::player::Player;

// 0 is an empty spot, 1 is player 1's spot, -1 is player -1's spot
pub struct Game {
    pub board: [i8; 9],
    pub current_player: i8,
    pub status: GameStatus
}

pub struct GameResult {
    pub player1: WinStatus,
    pub player2: WinStatus
}

#[derive(Debug)]
pub enum GameStatus {
    InProgress,
    Winner,
    Draw
}

pub enum WinStatus {
    Winner,
    Loser,
    Draw
}

impl Game {
    pub fn new() -> Game {
        Game {
            board: [0i8; 9],
            current_player: 1,
            status: GameStatus::InProgress
        }
    }

    pub fn play_game(&mut self, player1: &impl Player, player2: &impl Player) -> (GameResult, [i8; 9]) {
        loop {
            match
                match self.current_player {
                1 => self.play_turn(player1.take_turn(&self.board)),
                -1 => self.play_turn(player2.take_turn(&self.board)),
                _ => Err("Unknown Player.  This shouldn't happen.")
                } {
                    Err(_) => {},
                    Ok(_) => {
                        match self.status {
                            GameStatus::Winner => {
                                if self.current_player == 1 {
                                    let result = GameResult { player1: WinStatus::Winner, player2: WinStatus::Loser };
                                    break { (result, self.board) };
                                } else {
                                    let result = GameResult { player1: WinStatus::Loser, player2: WinStatus::Winner };
                                    break { (result, self.board) };
                                }
                            },
                            GameStatus::Draw => {
                                let result = GameResult { player1: WinStatus::Draw, player2: WinStatus::Draw };
                                break { (result, self.board) };
                            }
                            _ => {}
                        }
                    }
                }
        }
    }

    pub fn play_turn(&mut self, position: u8) -> Result<bool, &str> {
        if position > 8 || self.board[position as usize] != 0 {
            Err("Invalid position")
        } else {

            self.board[position as usize] = self.current_player;
            match check_for_winner(self) {
                GameStatus::InProgress => self.current_player = self.current_player * -1,
                GameStatus::Winner => {
                    self.status = GameStatus::Winner;
                },
                GameStatus::Draw => {
                    self.status = GameStatus::Draw;
                }
            }
            Ok(true)
        }
    }

}

fn check_for_winner(game: &Game) -> GameStatus {
    let mut winner = false;
    let mut draw = false;
    for i in 0..3 {
        if (game.board[i*3+0] + game.board[i*3+1] + game.board[i*3+2]).abs() == 3  ||
            (game.board[i] + game.board[i+3] + game.board[i+6]).abs() == 3 ||
            (game.board[i] + game.board[4] + game.board[8-i]).abs() == 3 {
                winner = true;
                break;
            }
    }
    if ! game.board.iter().any(|x| *x==0i8) {
        draw = true;
    } 

    if winner {
        GameStatus::Winner
    } else if draw {
        GameStatus::Draw
    } else {
        GameStatus::InProgress
    }
}


pub fn potential_win(board: &[i8; 9], player_number: i8) -> Option<u8> {
    let across: [fn(usize)->usize; 3] = [ |i| i*3, |i| i*3+1, |i| i*3+2 ];
    let down: [fn(usize)->usize; 3] = [ |i| i, |i| i+3, |i| i+6 ];
    let diagonal: [fn(usize)->usize; 3] = [ |i| i, |_i| 4, |i| 8-i ];

    match potential_win_translated(board, player_number, across) {
        Some(pos) => Some(pos),
        None =>
            match potential_win_translated(board, player_number, down) {
                Some(pos) => Some(pos),
                None =>
                    match potential_win_translated(board, player_number, diagonal) {
                        Some(pos) => Some(pos),
                            None => None
                    }
            }
    }
}

fn potential_win_translated(
    board: &[i8; 9],
    player_number: i8,
    translation: [fn(usize)->usize; 3])
    -> Option<u8> {

    let winning_row: Option<Vec<(usize, i8)>> = (0..3)
        .into_iter()
        .map(|row| vec![
            (translation[0](row), board[translation[0](row)]),
            (translation[1](row), board[translation[1](row)]),
            (translation[2](row), board[translation[2](row)]) ]
        )
        .filter(|row| ! row.iter().any(|(_pos, cell)| *cell == (player_number*-1)))
        .filter(|row| row.iter().map(|(_pos, cell)| cell.abs()).sum::<i8>() == 2)
        .find(|_row| true);

    match winning_row
    {
        Some(row) => row
            .iter()
            .filter(|(_pos, cell)| *cell == 0)
            .map(|(pos, _cell)| *pos as u8)
            .find(|_p| true),
        None => None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_play_turn_goes_to_next_player_after_valid_move() {
        let mut game = Game::new();
        match game.play_turn(4) { // middle square
            Err(_) => panic!("Error making move"),
            Ok(_) => {
                assert_eq!(game.current_player, -1);
                assert_eq!(game.board, [0, 0, 0, 0, 1, 0, 0, 0, 0])
            }
        }
    }

    #[test]
    fn test_check_for_winner_across() {
        let mut game = Game::new();
        game.board = [1, 1, 1,
                      0, 0, 0,
                      0, 0, 0];
        match check_for_winner(&game) {
            GameStatus::Winner => {}
            _ => panic!("Failed test")
        }

        game.board = [0, 0, 0,
                      1, 1, 1,
                      0, 0, 0];
        match check_for_winner(&game) {
            GameStatus::Winner => {}
            _ => panic!("Failed test")
        }

        game.board = [0, 0, 0,
                      0, 0, 0,
                      -1, -1, -1];
        match check_for_winner(&game) {
            GameStatus::Winner => {}
            _ => panic!("Failed test")
        }
    }

    #[test]
    fn test_check_for_winner_down() {
        let mut game = Game::new();
        game.board = [1, 0, 0,
                      1, 0, 0,
                      1, 0, 0];
        match check_for_winner(&game) {
            GameStatus::Winner => {}
            _ => panic!("Failed test")
        }

        game.board = [0, 1, 0,
                      0, 1, -1,
                      0, 1, 0];
        match check_for_winner(&game) {
            GameStatus::Winner => {}
            _ => panic!("Failed test")
        }

        game.board = [1,  0, -1,
                      0,  1, -1,
                      0, -1, -1];
        match check_for_winner(&game) {
            GameStatus::Winner => {}
            _ => panic!("Failed test")
        }
    }

    #[test]
    fn test_check_for_winner_diagonally() {
        let mut game = Game::new();
        game.board = [1, -1, -1,
                      -1, 1, -1,
                      1, -1, 1];
        match check_for_winner(&game) {
            GameStatus::Winner => {}
            _ => panic!("Failed test")
        }

        game.board = [0, 0, 1,
                      0, 1, -1,
                      1, 1, 0];
        match check_for_winner(&game) {
            GameStatus::Winner => {}
            _ => panic!("Failed test")
        }
    }

    #[test]
    fn test_check_for_winner_draw() {
        let mut game = Game::new();
        game.board = [ 1,-1, -1,
                       -1, 1,  1,
                       1,-1, -1];
        match check_for_winner(&game) {
            GameStatus::Draw => {}
            _ => panic!("Failed test")
        }
    }

    #[test]
    fn test_check_for_potential_win_across() {
        let mut game = Game::new();
        game.board = [1, 0, 1,
                      0, 0, 0,
                      0, 0, 0];
        match potential_win(&game.board, 1) {
            Some(pos) => assert_eq!(pos, 1),
            _ => panic!("Failed test")
        }

        game.board = [0, 0, 0,
                      1, 0, 1,
                      0, 0, 0];
        match potential_win(&game.board, 1) {
            Some(pos) => assert_eq!(pos, 4),
            _ => panic!("Failed test")
        }

        game.board = [0, 0, 0,
                      0, 0, 0,
                      -1, 0, -1];
        match potential_win(&game.board, -1) {
            Some(pos) => assert_eq!(pos, 7),
            _ => panic!("Failed test")
        }
    }

    #[test]
    fn test_check_for_potential_win_down() {
        let mut game = Game::new();
        game.board = [1, 0, 0,
                      0, 0, 0,
                      1, 0, 0];
        match potential_win(&game.board, 1) {
            Some(pos) => assert_eq!(pos, 3),
            _ => panic!("Failed test")
        }

        game.board = [0, 1, 0,
                      0, 0, -1,
                      0, 1, 0];
        match potential_win(&game.board, 1) {
            Some(pos) => assert_eq!(pos, 4),
            _ => panic!("Failed test")
        }

        game.board = [1,  0, -1,
                      0,  1, 0,
                      1, -1, -1];
        match potential_win(&game.board, -1) {
            Some(pos) => assert_eq!(pos, 5),
            _ => panic!("Failed test")
        }
    }

    #[test]
    fn test_check_for_potential_win_diagonally() {
        let mut game = Game::new();
        game.board = [1, -1, -1,
                      1,  0, -1,
                      1, -1, 1];
        match potential_win(&game.board, 1) {
            Some(pos) => assert_eq!(pos, 4),
            _ => panic!("Failed test")
        }

        game.board = [ 0, 0, 0,
                       0, 1, -1,
                       1, 0, -1];
        match potential_win(&game.board, 1) {
            Some(pos) => assert_eq!(pos, 2),
            _ => panic!("Failed test")
        }
    }

    #[test]
    fn test_check_for_winner_in_progress() {
        let mut game = Game::new();
        game.board = [1,  0,  0,
                      0,  1, -1,
                      1, -1, -1];
        match check_for_winner(&game) {
            GameStatus::InProgress => {}
            x => panic!("Failed test, got: {:?}", x)
        }
    }


}


